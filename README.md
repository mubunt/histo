 # *histo*, Calculation and histogram display.

**histo** is a small application that calculates the appearance frequencies of numbers read in a user file and displays the histogram. in a terminal type window.

## LICENSE
**histo** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ histo -h
histo - Copyright (c) 2018, cMichel RIZZO. All Rights Reserved.
histo - Version 1.0.0

Calculation and histogram display

Usage: histo [OPTIONS]...

  -h, --help             Print help and exit
  -V, --version          Print version and exit
  -v, --verbose          Verbose mode  (default=off)
  -b, --bins=LONG        Number of bins
  -s, --size=DOUBLE      Bin size
  -x, --abscissa=STRING  Abscissa label  (default=`')
  -y, --ordinate=STRING  Ordinate label  (default=`Frequency')
  -f, --file=FILENAME    Data file
  -m, --monochrome       Monochrome mode  (default=off)

Exit: returns a non-zero status if an error is detected.

$ histo -V
histo - Copyright (c) 2018, cMichel RIZZO. All Rights Reserved.
histo - Version 1.0.0
$ histo --file=examples/inputdata01.txt -abscissa=""Weight in kilo"" --ordinate=""Number of rations""
...
```

## BIN SIZE
By default (that means that options *--bin* and *--size* (previously *--width*) are not present), the size of bins is givent by the Sturges' formula. Itis derived from a binomial distribution and implicitly assumes an approximately normal distribution.

> k = 1 + log{2}n

It implicitly bases the bin sizes on the range of the data and can perform poorly if n < 30, because the number of bins will be small—less than seven—and unlikely to show trends in the data well. It may also perform poorly if the data are not normally distributed.

## EXAMPLES
These examples are available in the *examples* directory by running the *examples* scripts.

![Example 1](./README_images/histo01.png  "Example 1")

![Example 2](./README_images/histo02.png  "Example 2")

![Example 3](./README_images/histo03.png  "Example 3")

![Example 4](./README_images/histo04.png  "Example 4")

![Example 5](./README_images/histo05.png  "Example 5")

![Example 6](./README_images/histo06.png  "Example 6")

## STRUCTURE OF THE APPLICATION
This section walks you through **histo**'s structure. Once you understand this structure, you will easily find your way around in **histo**'s code base.

``` bash
$ yaTree
./                      # Application level
├── README_images/      # Images for documentation
│   ├── histo01.png     # 
│   ├── histo02.png     # 
│   ├── histo03.png     # 
│   ├── histo04.png     # 
│   ├── histo05.png     # 
│   └── histo06.png     # 
├── examples/           # Example directory
│   ├── examples        # Bash script ti run all examples
│   ├── inputdata01.txt # Example with all the positive data (one per line)
│   ├── inputdata02.txt # Example with all the positive data (a batch per line)
│   ├── inputdata03.txt # Example with all the positive data, starting at 0.
│   ├── inputdata04.txt # Example with all the positive and negative data
│   └── inputdata05.txt # Example with all the negative data
├── src/                # Source directory
│   ├── Makefile        # Makefile
│   ├── histo.c         # Main program
│   ├── histo.ggo       # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
│   ├── histo.h         # Internal header file
│   └── histo_display.c # Routine to display histogram
├── COPYING.md          # GNU General Public License markdown file		
├── LICENSE.md          # License markdown file
├── Makefile            # Makefile
├── README.md           # ReadMe markdown file
├── RELEASENOTES.md     # Release Notes markdown file
└── VERSION             # Version identification text file

3 directories, 22 files

$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd histo
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd histo
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```

## NOTES

## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.22.6.
- Developped and tested on XUBUNTU 18.04, GCC v7.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***