//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: histo
// Calculation and histogram display
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "histo.h"
#include "histo_cmdline.h"
//------------------------------------------------------------------------------
// MACROS DEFINITION
//------------------------------------------------------------------------------
#define VERBOSE						args_info.verbose_given
#define PREFIX_FATAL				"FATAL ERROR"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define EXIST(x)					(stat(x, ptlocstat)<0 ? 0:locstat.st_mode)
#define EXISTDIR(y)					(EXIST(y) && (locstat.st_mode & S_IFMT) == S_IFDIR)
#define EXISTFILE(y)				(EXIST(y) && (locstat.st_mode & S_IFMT) == S_IFREG)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define MAX(a,b)					(a > b ? a : b)
#define MIN(a,b)					(a > b ? b : a)
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info	args_info;
static char 						*pending_error = NULL;
static int 							nbLINES;
static int 							nbCOLS;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void error( const char *format, const char *prefix, ... ) {
	va_list argp;
	char buff[512];
	va_start(argp, prefix);
	vsprintf(buff, format, argp);
	va_end(argp);
	fprintf(stderr, "\n%s: %s.\n\n", prefix, buff);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool getTerminalSize( int *lines, int *cols ) {
	struct winsize w;
	if (0 != ioctl(STDOUT_FILENO, TIOCGWINSZ, &w)) {
		pending_error = malloc(256 * (sizeof(char)));
		sprintf(pending_error, "Cannot get terminal size");
		return false;
	}
	*lines = w.ws_row;
	*cols = w.ws_col;
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static double *readingdatafile( char *file, size_t *n, double *min, double *max) {
	double *array = NULL;
	*min = DBL_MAX;
	*max = -DBL_MAX;
	FILE *fd;
	fd = fopen(file, "r");
	if (fd == NULL) {
		pending_error = malloc(256 * (sizeof(char)));
		sprintf(pending_error, "Cannot open file '%s'", file);
		return array;
	}
	size_t allocated_size = 10;
	array = (double *) malloc (allocated_size * sizeof(double));
	*n = 0;
	double val;
	while (1 == fscanf(fd, "%lf", &val)) {
		if (*n >= allocated_size) {
			allocated_size += allocated_size;
			array = (double *) realloc(array, allocated_size * sizeof(double));
		}
		array[*n] = val;
		*max = MAX(*max, val);
		*min = MIN(*min, val);
		++*n;
	}
	fclose(fd);
	return array;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static unsigned int computebins( size_t n ) {
	// Herbert Sturges' formula: k = 1 + Log2(n)
	unsigned int k = (unsigned int) (1. + log2((double) n));
	return k;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void computedistribution( double vals[], size_t n,
                                 double min, double max, double size,
                                 s_distrib *distribution, unsigned int nbins,
                                 unsigned int *maxdistrib ) {
	distribution[0].min = min;
	distribution[0].max = distribution[0].min + size;
	distribution[0].number = 0;
	for (unsigned int i = 1; i < nbins; i++) {
		distribution[i].min = distribution[i - 1].min + size;
		distribution[i].max = distribution[i - 1].max + size;
		distribution[i].number = 0;
	}

	*maxdistrib = 0;
	for (size_t j = 0; j < n; j++) {
		unsigned int  i;
		for (i = 0; i < nbins; i++) {
			if (vals[j] >= distribution[i].min && vals[j] < distribution[i].max) {
				++distribution[i].number;
				*maxdistrib = MAX(*maxdistrib, distribution[i].number);
				break;
			}
		}
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void ssdata_analysis( char *val, int *f, int *d ) {
	char *pt = val + strlen(val) - 1;
	while (*pt == '0' && *pt != '.') --pt;
	if (*pt == '.') {
		++pt;
		*pt = '0';
	}
	++pt;
	*pt = '\0';
	*f = MAX(*f, (int) strlen(val));
	pt = val;
	while (*pt != '.' && *pt != '\0') ++pt;
	if (*pt == '.') *d = MAX(*d, (int) ((val  + strlen(val) - 1) - pt));
}
static void data_analysis( s_distrib *distribution, size_t n, double size, int *f, int *d ) {
	char strval[64];
	*f = *d = 0;
	for (unsigned int i = 0; i < n; i++) {
		sprintf(strval, "%f", distribution[i].min);
		ssdata_analysis(strval, f, d);
		sprintf(strval, "%f", distribution[i].max);
		ssdata_analysis(strval, f, d);
	}
	sprintf(strval, "%f", distribution[n - 1].max + size);
	ssdata_analysis(strval, f, d);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int frqanalysys( unsigned int freq ) {
	char strval[32];
	sprintf(strval, "%d", freq);
	return (int) strlen(strval);
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct stat	locstat;			// stat variables for EXIST*
	struct stat	*ptlocstat;			// stat variables for EXIST*
	s_distrib *ptdistrib = NULL;
	double *ptfigures = NULL;
	double sizeperbin, minfigure, maxfigure;
	unsigned int numberofbins = 0, maxfrequency = 0;;
	int report, format_length, format_decimal, format_freq, colsperbin;
	size_t numberoffigures = 0;
	char *buffline = NULL;
	//---- Initialization ------------------------------------------------------
	ptlocstat = &locstat;
	signal(SIGABRT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	signal(SIGINT, SIG_IGN);
	signal(SIGWINCH, SIG_IGN);
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_histo(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	if (! EXISTFILE(args_info.file_arg)) {
		error("Data file '%s' does not exist.", PREFIX_FATAL, args_info.file_arg);
		goto ERROR;
	}
	if (args_info.bins_given && args_info.bins_arg <= 0) {
		error("Wrong number of bins (%ld).", PREFIX_FATAL, args_info.bins_arg);
		goto ERROR;
	}
	if (args_info.size_given && args_info.size_arg <= 0) {
		error("Wrong number of size (%ld).", PREFIX_FATAL, args_info.size_arg);
		goto ERROR;
	}
	if (args_info.bins_given && args_info.size_given) {
		error("Options '--bins/-b' and '-size/-s' can not be used together", PREFIX_FATAL);
		goto ERROR;
	}
	//----  Go on --------------------------------------------------------------
	ptfigures = readingdatafile(args_info.file_arg, &numberoffigures, &minfigure, &maxfigure);
	if (ptfigures == NULL) {
		error("%s", PREFIX_FATAL, pending_error);
		goto ERROR;
	}

	if (args_info.bins_given) {
		numberofbins = (unsigned int) args_info.bins_arg;
		sizeperbin = (maxfigure - minfigure) / (double) numberofbins;
	} else {
		if (args_info.size_given) {
			sizeperbin = args_info.size_arg;
			numberofbins = (unsigned int) ((maxfigure - minfigure) / (double) sizeperbin);
		} else {
			numberofbins = computebins(numberoffigures);
			sizeperbin = (maxfigure - minfigure) / (double) numberofbins;
		}
	}

	ptdistrib = (s_distrib *) malloc((numberofbins + 1) * sizeof(s_distrib));
	computedistribution(ptfigures, numberoffigures, minfigure, maxfigure, sizeperbin, ptdistrib, numberofbins, &maxfrequency);
	data_analysis(ptdistrib, numberofbins, sizeperbin, &format_length, &format_decimal);
	format_freq = frqanalysys(maxfrequency);

	if (VERBOSE) {
		fprintf(stdout, "Data file .................... %s\n", args_info.file_arg);
		fprintf(stdout, "Number of figures ............ %ld\n", numberoffigures);
		fprintf(stdout, "Minimal value ................ %lf\n", minfigure);
		fprintf(stdout, "Maximal value ................ %lf\n", maxfigure);
		if (args_info.bins_given)
			fprintf(stdout, "Requested number of bins ..... %d\n", numberofbins);
		else
			fprintf(stdout, "Computed number of bins ...... %d\n", numberofbins);
		fprintf(stdout, "Width per bin ................ %lf\n", sizeperbin);
		fprintf(stdout, "Distribution ................\n");
		for (unsigned int _i = 0; _i < numberofbins; _i++) {
			fprintf(stdout, "	Bin %d: %*.*lf - %*.*lf	%d", _i, format_length, format_decimal, ptdistrib[_i].min,
			        format_length, format_decimal, ptdistrib[_i].max, ptdistrib[_i].number);
			if (ptdistrib[_i].number == maxfrequency)
				fprintf(stdout, " *");
			fprintf(stdout, "\n");
		}
	}

	if (! getTerminalSize(&nbLINES, &nbCOLS)) {
		error("%s", PREFIX_FATAL, pending_error);
		goto ERROR;
	}

	display(args_info.monochrome_given, nbCOLS, ptdistrib, numberofbins, args_info.ordinate_arg, args_info.abscissa_arg,
	        maxfrequency, format_freq, format_length, format_decimal);
	//---- Exit ----------------------------------------------------------------
	report = EXIT_SUCCESS;
EXIT:
//	if (buffline != NULL) free(buffline);
	if (ptfigures != NULL) free(ptfigures);
	if (ptdistrib != NULL) free(ptdistrib);
	cmdline_parser_histo_free(&args_info);
	signal(SIGABRT, SIG_DFL);
	signal(SIGTERM, SIG_DFL);
	signal(SIGINT, SIG_DFL);
	signal(SIGWINCH, SIG_DFL);
	return report;
ERROR:
	if (pending_error != NULL) free(pending_error);
	report = EXIT_FAILURE;
	goto EXIT;
}
//------------------------------------------------------------------------------
