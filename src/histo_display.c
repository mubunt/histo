//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: histo
// Calculation and histogram display
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "histo.h"
//------------------------------------------------------------------------------
// MACROS DEFINITION
//------------------------------------------------------------------------------
#define ESC_ATTRIBUTSOFF			"\033[0m"
#define ESC_STRONG_ON				"\033[1m"
#define ESC_COLOR					"\033[%dm"
#define ESC_MOVEUP					"\033[%dA"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Colored
#define HORIZONTAL					"_"
#define VERTICAL					"│"
#define SPACE						" "
#define BACKGROUND(color) 			(40 + color)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Monochrome
#define M_TOPLEFTCORNER 			"┌"
#define M_TOPRIGHTCORNER 			"┐"
#define M_HORIZONTAL 				"─"
#define M_AXIS_HORIZONTAL 			"━"
#define M_VERTICAL 					"│"
#define M_BOTTOMLEFTCORNER 			"┿"
#define M_BOTTOMRIGHTCORNER			"┻"
#define M_AXIS_CORNER				"┷"
#define M_YAXIS						"┫"
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static int computebinsize( int nbcols, int taxchars, unsigned int nbbins ) {
	return (nbcols - taxchars) / (int) nbbins;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void display_colored( s_distrib *bins, int nbcolumnsperbin, unsigned int nbbins,
                             unsigned int maxfrequency, int format_frequence, int length_values, int nbdecimal_values,
                             char *ylabel, char *xlabel ) {
	char line[512];
	char buffxaxis[512];
	char strval[32];
	unsigned int i, ii;
	int j, k;
	char *background = malloc((size_t) (4 * nbcolumnsperbin + 1) * sizeof(char));
	//---- Background line
	*background = '\0';
	for (int i = 0; i < nbcolumnsperbin; i++) {
		if (0 == i % 2) strcat(background, HORIZONTAL);
		else strcat(background, SPACE);
	}
	//---- Histogram
	fprintf(stdout, "\n%s\n", ylabel);

	sprintf(line, "%*s", nbcolumnsperbin, SPACE);
	for (k = (int) maxfrequency + 1; k > 0; k--) {
		if (0 == k % 2)
			fprintf(stdout, "%*d " ESC_STRONG_ON "%s%s" ESC_ATTRIBUTSOFF, format_frequence, k, HORIZONTAL, VERTICAL);
		else
			fprintf(stdout, "%*s " ESC_STRONG_ON "%s%s" ESC_ATTRIBUTSOFF, format_frequence, SPACE, HORIZONTAL, VERTICAL);
		for (ii = 0; ii < nbbins; ii++) {
			if (k < (int) bins[ii].number) {
				fprintf(stdout, ESC_STRONG_ON ESC_COLOR "%s" ESC_ATTRIBUTSOFF, BACKGROUND(1 + (ii % 7)), line);
			} else {
				fprintf(stdout, "%s", background);
			}
		}
		fprintf(stdout, "%s\n", background);
	}
	//---- Bottom of histogramme
	*buffxaxis = '\0';
	for (i = 0; i < (unsigned int) nbcolumnsperbin; i++) strcat(buffxaxis, HORIZONTAL);
	fprintf(stdout, "%*d " ESC_STRONG_ON "%s%s" ESC_ATTRIBUTSOFF, format_frequence, 0, HORIZONTAL, VERTICAL);
	int length_axis = format_frequence + 2;
	for (ii = 0; ii < nbbins; ii++) {
		if (bins[ii].number != 0)
			fprintf(stdout, ESC_STRONG_ON ESC_COLOR "%s" ESC_ATTRIBUTSOFF, BACKGROUND(1 + (ii % 7)), buffxaxis);
		else
			fprintf(stdout, ESC_STRONG_ON "%s" ESC_ATTRIBUTSOFF, buffxaxis);
		length_axis += nbcolumnsperbin;
	}
	fprintf(stdout, ESC_STRONG_ON "%s" ESC_ATTRIBUTSOFF "\n", buffxaxis);
	length_axis += nbcolumnsperbin;
	//---- X axis
	sprintf(line, "%*s", format_frequence + 2, " ");
	for (i = 0; i < nbbins; i++) {
		sprintf(strval, "%*.*lf", length_values, nbdecimal_values, bins[i].min);
		strcat(line, strval);
		for (j = length_values; j < nbcolumnsperbin; j++) strcat(line, SPACE);
	}
	sprintf(strval, "%*.*lf", length_values, nbdecimal_values, bins[i - 1].max);
	strcat(line, strval);
	fprintf(stdout, "%s\n", line);
	// X legend
	*line = '\0';
	for (j = 0; j <  (length_axis - (int) strlen(xlabel) + 1); j++) strcat(line, SPACE);
	fprintf(stdout, "%s%s\n\n", line, xlabel);
	free(background);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void display_monochrom( s_distrib *bins, int nbcolumnsperbin, unsigned int nbbins,
                               unsigned int maxfrequency, int format_frequence, int length_values, int nbdecimal_values,
                               char *ylabel, char *xlabel ) {
	char line1[512];
	char line2[512];
	char buffxaxis[512];
	char strval[32];
	unsigned int i, ii;
	int j, k;
	char *background = malloc((size_t) (4 * nbcolumnsperbin + 1) * sizeof(char) * 4);
	//---- Background line
	*background = '\0';
	for (int i = 0; i < nbcolumnsperbin; i++) {
		if (0 == i % 2) strcat(background, M_HORIZONTAL);
		else strcat(background, SPACE);
	}
	//---- Histogram
	fprintf(stdout, "\n%s\n", ylabel);

	sprintf(line1, "%s%*s%s", M_VERTICAL, (nbcolumnsperbin - 2), SPACE, M_VERTICAL);
	strcpy(line2, M_TOPLEFTCORNER);
	for (k = 0; k < nbcolumnsperbin - 2; k++) strcat(line2, M_HORIZONTAL);
	strcat(line2, M_TOPRIGHTCORNER);

	for (k = (int) maxfrequency + 1; k > 0; k--) {
		if (0 == k % 2)
			fprintf(stdout, "%*d %s", format_frequence, k, M_YAXIS);
		else
			fprintf(stdout, "%*s %s", format_frequence, SPACE, M_YAXIS);
		for (ii = 0; ii < nbbins; ii++) {
			if (k == (int) bins[ii].number)
				fprintf(stdout, "%s", line2);
			else if (k < (int) bins[ii].number)
				fprintf(stdout, "%s", line1);
			else
				fprintf(stdout, "%s", background);
		}
		fprintf(stdout, "%s\n", background);
	}
	//---- Bottom of histogramme
	strcpy(buffxaxis, M_BOTTOMLEFTCORNER);
	for (i = 1; i < (unsigned int) nbcolumnsperbin - 1; i++) strcat(buffxaxis, M_AXIS_HORIZONTAL);
	strcat(buffxaxis, M_AXIS_CORNER);
	fprintf(stdout, "%*d %s", format_frequence, 0, M_AXIS_CORNER);
	int length_axis = format_frequence + 2;
	for (ii = 0; ii < nbbins; ii++) {
		fprintf(stdout,"%s", buffxaxis);
		length_axis += nbcolumnsperbin;
	}
	fprintf(stdout,"%s\n", buffxaxis);
	length_axis += nbcolumnsperbin;
	//---- X axis
	sprintf(line1, "%*s", format_frequence + 2, " ");
	for (i = 0; i < nbbins; i++) {
		sprintf(strval, "%*.*lf", length_values, nbdecimal_values, bins[i].min);
		strcat(line1, strval);
		for (j = length_values; j < nbcolumnsperbin; j++) strcat(line1, SPACE);
	}
	sprintf(strval, "%*.*lf", length_values, nbdecimal_values, bins[i - 1].max);
	strcat(line1, strval);
	fprintf(stdout, "%s\n", line1);
	// X legend
	*line1 = '\0';
	for (j = 0; j <  (length_axis - (int) strlen(xlabel) + 1); j++) strcat(line1, SPACE);
	fprintf(stdout, "%s%s\n\n", line1, xlabel);
	free(background);
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTION
//------------------------------------------------------------------------------
void display( unsigned int monochrom, int nbcolumns, s_distrib *bins, unsigned int nbbins, char *ylabel, char *xlabel,
              unsigned int maxfrequency, int format_frequence, int length_values, int nbdecimal_values) {
	int nbcolumnsperbin = computebinsize(nbcolumns,
	                                     format_frequence + 3,		// Frequency and Y axis
	                                     nbbins + 1);
	//---- Histogram
	if (monochrom)
		display_monochrom(bins, nbcolumnsperbin, nbbins, maxfrequency, format_frequence, length_values, nbdecimal_values, ylabel, xlabel);
	else
		display_colored(bins, nbcolumnsperbin, nbbins, maxfrequency, format_frequence, length_values, nbdecimal_values, ylabel, xlabel);
}
//------------------------------------------------------------------------------
